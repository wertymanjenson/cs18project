#include <Windows.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <assert.h>
#include <math.h>
#include "MyForm.h"

#include "eyex/EyeX.h"

#pragma comment (lib, "Tobii.EyeX.Client.lib")

using namespace System;
using namespace System::Windows::Forms;
[STAThread]
void main(array<String^>^ args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	BrowserTess::MyForm form;
	Application::Run(%form);

}