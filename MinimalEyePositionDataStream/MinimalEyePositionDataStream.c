

#include <Windows.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <assert.h>
#include <math.h>
#include "eyex/EyeX.h"
typedef int bool;
#define true 1
#define false 0

#pragma comment (lib, "Tobii.EyeX.Client.lib")

// ID of the global interactor that provides our data stream; must be unique within the application.
static const TX_STRING InteractorId = "Fluttershy";

// global variables
static TX_HANDLE g_hGlobalInteractorSnapshot = TX_EMPTY_HANDLE;
static float g_avgDistance = 0;
static time_t g_time;
static int signalsCaught = 0;
static bool g_SignalRecieved = false;

/*
 * Initializes g_hGlobalInteractorSnapshot with an interactor that has the Eye Position behavior.
 */
BOOL InitializeGlobalInteractorSnapshot(TX_CONTEXTHANDLE hContext)
{
	TX_HANDLE hInteractor = TX_EMPTY_HANDLE;
	TX_HANDLE hBehaviorWithoutParameters = TX_EMPTY_HANDLE;

	BOOL success;

	success = txCreateGlobalInteractorSnapshot(
		hContext,
		InteractorId,
		&g_hGlobalInteractorSnapshot,
		&hInteractor) == TX_RESULT_OK;
	success &= txCreateInteractorBehavior(hInteractor, &hBehaviorWithoutParameters, TX_BEHAVIORTYPE_EYEPOSITIONDATA) == TX_RESULT_OK;

	txReleaseObject(&hInteractor);

	return success;
}

/*
 * Callback function invoked when a snapshot has been committed.
 */
void TX_CALLCONVENTION OnSnapshotCommitted(TX_CONSTHANDLE hAsyncData, TX_USERPARAM param)
{
	// check the result code using an assertion.
	// this will catch validation errors and runtime errors in debug builds. in release builds it won't do anything.

	TX_RESULT result = TX_RESULT_UNKNOWN;
	txGetAsyncDataResultCode(hAsyncData, &result);
	assert(result == TX_RESULT_OK || result == TX_RESULT_CANCELLED);
}

/*
 * Callback function invoked when the status of the connection to the EyeX Engine has changed.
 */
void TX_CALLCONVENTION OnEngineConnectionStateChanged(TX_CONNECTIONSTATE connectionState, TX_USERPARAM userParam)
{
	switch (connectionState) {
	case TX_CONNECTIONSTATE_CONNECTED: {
			BOOL success;
			printf("The connection state is now CONNECTED (We are connected to the EyeX Engine)\n");
			// commit the snapshot with the global interactor as soon as the connection to the engine is established.
			// (it cannot be done earlier because committing means "send to the engine".)
			success = txCommitSnapshotAsync(g_hGlobalInteractorSnapshot, OnSnapshotCommitted, NULL) == TX_RESULT_OK;
			if (!success) {
				printf("Failed to initialize the data stream.\n");
			}
			else {
				printf("Waiting for eye position data to start streaming...\n");
			}
		}
		break;

	case TX_CONNECTIONSTATE_DISCONNECTED:
		printf("The connection state is now DISCONNECTED (We are disconnected from the EyeX Engine)\n");
		break;

	case TX_CONNECTIONSTATE_TRYINGTOCONNECT:
		printf("The connection state is now TRYINGTOCONNECT (We are trying to connect to the EyeX Engine)\n");
		break;

	case TX_CONNECTIONSTATE_SERVERVERSIONTOOLOW:
		printf("The connection state is now SERVER_VERSION_TOO_LOW: this application requires a more recent version of the EyeX Engine to run.\n");
		break;

	case TX_CONNECTIONSTATE_SERVERVERSIONTOOHIGH:
		printf("The connection state is now SERVER_VERSION_TOO_HIGH: this application requires an older version of the EyeX Engine to run.\n");
		break;
	}
}

/*
 * Handles an event from the Eye Position data stream.
 */
void OnEyePositionDataEvent(TX_HANDLE hEyePositionDataBehavior)
{
	COORD position = {0,8};
	float newAvg = 0;
	
	
	TX_EYEPOSITIONDATAEVENTPARAMS eventParams;
	if (txGetEyePositionDataEventParams(hEyePositionDataBehavior, &eventParams) == TX_RESULT_OK) { 
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position); 


			/* CODE START
			 * calculate and average distance from screen the user is expected to be. When the user leans in signal that they are doing so
			 * in order to activate magnification.
			 */
			if ((time(NULL)- g_time) >= 1){ //if a second has passed
				if (!g_SignalRecieved){
					g_SignalRecieved = true;
					//g_time = time(NULL);
					if (eventParams.LeftEyeX != 0 && eventParams.RightEyeX != 0){
						if (g_avgDistance == 0){
							g_avgDistance = (eventParams.LeftEyeZ + eventParams.RightEyeZ) / 2.0;
						}
						else if (eventParams.LeftEyeZ != 0.0 && eventParams.RightEyeZ != 0.0) {
							g_time = time(NULL);
							newAvg = (eventParams.LeftEyeZ + eventParams.RightEyeZ) / 2.0;

							/*if ((newAvg > (g_avgDistance - 30.0)) && (newAvg < (g_avgDistance + 30.0))){
								g_avgDistance = (g_avgDistance + newAvg) / 2.0;
							}
							else*/ if ((newAvg != 0) && (g_avgDistance - newAvg) > 30){
								g_avgDistance = newAvg;
								//printf("We would zoom since difference in movement from avg was: %.1f mm\n",
								//	fabs(g_avgDistance - newAvg));
								printf("Number of signals caught %i\n", ++signalsCaught);
								FILE *f = fopen("tmp.txt", "w");
								if (f == NULL)
								{
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									exit(1);
								}

								/* print some text */
								const int num = 1;
								fprintf(f, "%i\n", num);
								printf("signal value: %i\n", num);
								fclose(f);
							}
							else if ((newAvg != 0) && (g_avgDistance - newAvg) < -30){
								g_avgDistance = newAvg;
								//printf("We would zoom since difference in movement from avg was: %.1f mm\n",
								//	fabs(g_avgDistance - newAvg));
								printf("Number of signals caught %i\n", ++signalsCaught);
								FILE *f = fopen("tmp.txt", "w");
								if (f == NULL)
								{
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									printf("Error opening file!\n");
									exit(1);
								}

								/* print some text */
								const int num = -1;
								fprintf(f, "%d\n", num);
								printf("signal value: %d\n", num);
								fclose(f);
							}
						}
					}
					g_SignalRecieved = false;
				}
			}
	} else {
		printf("Failed to interpret eye position data event packet.\n");
		}
}

/*
 * Callback function invoked when an event has been received from the EyeX Engine.
 */
void TX_CALLCONVENTION HandleEvent(TX_CONSTHANDLE hAsyncData, TX_USERPARAM userParam)
{
	TX_HANDLE hEvent = TX_EMPTY_HANDLE;
	TX_HANDLE hBehavior = TX_EMPTY_HANDLE;

	txGetAsyncDataContent(hAsyncData, &hEvent);

	// NOTE. Uncomment the following line of code to view the event object. The same function can be used with any interaction object.
	//OutputDebugStringA(txDebugObject(hEvent));

	if (txGetEventBehavior(hEvent, &hBehavior, TX_BEHAVIORTYPE_EYEPOSITIONDATA) == TX_RESULT_OK) {
		OnEyePositionDataEvent(hBehavior);
		txReleaseObject(&hBehavior);
	}

	// NOTE since this is a very simple application with a single interactor and a single data stream, 
	// our event handling code can be very simple too. A more complex application would typically have to 
	// check for multiple behaviors and route events based on interactor IDs.

	txReleaseObject(&hEvent);
}

/*
 * Application entry point.
 */

int main(int argc, char* argv[])
{
	TX_CONTEXTHANDLE hContext = TX_EMPTY_HANDLE;
	TX_TICKET hConnectionStateChangedTicket = TX_INVALID_TICKET;
	TX_TICKET hEventHandlerTicket = TX_INVALID_TICKET;
	BOOL success;

	// initialize and enable the context that is our link to the EyeX Engine.
	success = txInitializeEyeX(TX_EYEXCOMPONENTOVERRIDEFLAG_NONE, NULL, NULL, NULL, NULL) == TX_RESULT_OK;
	success &= txCreateContext(&hContext, TX_FALSE) == TX_RESULT_OK;
	success &= InitializeGlobalInteractorSnapshot(hContext);
	success &= txRegisterConnectionStateChangedHandler(hContext, &hConnectionStateChangedTicket, OnEngineConnectionStateChanged, NULL) == TX_RESULT_OK;
	success &= txRegisterEventHandler(hContext, &hEventHandlerTicket, HandleEvent, NULL) == TX_RESULT_OK;
	success &= txEnableConnection(hContext) == TX_RESULT_OK;

	// let the events flow until a key is pressed.
	if (success) {
		printf("Initialization was successful.\n");
	} else {
		printf("Initialization failed.\n");
	}
	printf("Press any key to exit...\n");

	///////////////////////////////////////////////////////////////










	_getch();
	printf("Exiting.\n");

	// disable and delete the context.
	txDisableConnection(hContext);
	txReleaseObject(&g_hGlobalInteractorSnapshot);
	success = txShutdownContext(hContext, TX_CLEANUPTIMEOUT_DEFAULT, TX_FALSE) == TX_RESULT_OK;
	success &= txReleaseContext(&hContext) == TX_RESULT_OK;
	success &= txUninitializeEyeX() == TX_RESULT_OK;
	if (!success) {
		printf("EyeX could not be shut down cleanly. Did you remember to release all handles?\n");
	}

	return 0;
}
