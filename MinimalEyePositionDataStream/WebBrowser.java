import javafx.scene.image.Image;
import java.util.Scanner;
import java.io.*;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import javafx.animation.PauseTransition;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class WebBrowser extends Application {

	private Scene scene;
	private BorderPane root;
	private Button reloadButton, goButton, backButton, forwardButton,
			setButton, testButton, resetButton, test2Button;
	private ComboBox<String> bookmarksComboBox;
	private TextField addressField;
	private WebView webView;
	private WebEngine webEngine;

	private String homeAddress = "www.google.com";
	private ArrayList<String> addresses = new ArrayList<String>();
	private int addressPointer = -1;
	Slider slider = new Slider(0.5,2,1);
	Double thumb=1.0;
	ZoomingDetection handler=new ZoomingDetection();
	Thread th = new Thread(handler);

	@Override
	public void start(Stage stage) throws Exception {
		System.out.println(Thread.currentThread().getName());

		// Horizontal boxes that will host buttons and address field.
		HBox addr = new HBox(5);
		addr.setAlignment(Pos.CENTER);
		VBox buttons = new VBox(5);
		VBox vBox0 = new VBox(5);
		vBox0.setAlignment(Pos.CENTER);

		// Buttons for navigation.
		reloadButton = new Button();
		reloadButton.setGraphic(new ImageView(new Image(getClass()
				.getResourceAsStream("resources/Refresh32.gif"))));
		reloadButton.setPrefSize(150, 150);
		goButton = new Button("Go");
		//goButton.setPrefSize(100, 100);
		backButton = new Button();
		backButton.setGraphic(new ImageView(new Image(getClass()
				.getResourceAsStream("resources/Back32.gif"))));
		backButton.setPrefSize(150, 150);
		forwardButton = new Button();
		forwardButton.setGraphic(new ImageView(new Image(getClass()
				.getResourceAsStream("resources/Forward32.gif"))));
		forwardButton.setPrefSize(150, 150);
		
		setButton = new Button(); //control slider bar
		resetButton= new Button(); //control slider bar
		testButton = new Button("Detection Off");
		test2Button = new Button("Detection On");
		//setButton.setGraphic(new ImageView(new Image(getClass()
		//		.getResourceAsStream("resources/Forward32.gif"))));
		testButton.setPrefSize(150, 150);
		test2Button.setPrefSize(150,150);

		// Add listeners to the buttons.
		reloadButton.setOnAction(reload);
		goButton.setOnAction(go);
		backButton.setOnAction(back);
		forwardButton.setOnAction(forward);
		
		setButton.setOnAction(set);
		testButton.setOnAction(test);
		resetButton.setOnAction(reset);
		test2Button.setOnAction(test2);

		// The TextField for entering URLs.
		addressField = new TextField("Enter URLs here...");
		addressField.setPrefColumnCount(50);
		addressField.setOnAction(go);
		
		VirtualKeyboard vkb = new VirtualKeyboard();
	    vkb.view().setStyle("-fx-border-color: darkblue; -fx-border-radius: 5;");
	    //vkb.view().disableProperty().bind(disabledCheckBox.selectedProperty());

		// Add buttons
		addr.getChildren().addAll(addressField, goButton,vkb.view());
		buttons.getChildren().addAll(backButton, forwardButton, reloadButton, testButton, test2Button);
		//hBox1.getChildren().addAll(vkb.view(),testButton);
		//vBox0.getChildren().addAll(vkb.view(),slider);
		//vBox0.getChildren().addAll(slider);

		// WebView that displays the page.
		webView = new WebView();
		ZoomingPane zoomingPane = new ZoomingPane(webView);
		zoomingPane.zoomFactorProperty().bind(slider.valueProperty());

		// The engine that manages the pages.
		webEngine = webView.getEngine();
		webEngine.setJavaScriptEnabled(true);
		homeAddress = extractAddress(homeAddress);
		webEngine.load("http://" + homeAddress);
		
		
		
		BorderPane root = new BorderPane(zoomingPane, null, null, slider, null);
		root.setPrefSize(1024, 768);

		// Add every node into the BorderPane.
		root.setTop(addr);
		//root.setBottom(vBox0);
		root.setLeft(buttons);
	//	root.setCenter(webView);
		

		// The scene is where all the actions in JavaFX take place. A scene
		// holds
		// all Nodes, whose root node is the BorderPane.
		scene = new Scene(root);

		// the stage hosts the scene.
		stage.setTitle("Web Browser");
		stage.setScene(scene);

		loadRandomAddress(homeAddress);
		stage.show();
	}
	
	public class ZoomingDetection extends Task
	{   
	    @Override
	    protected Integer call()
	    {
			while(true){
			try{
				boolean zoomed=false;
				FileReader fileReader = new FileReader("tmp.txt");
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String line=bufferedReader.readLine();
				if (line.equals("1")){
					setButton.fire();
					zoomed=true;
				}
				else if (line.equals("-1")){
					resetButton.fire();
					zoomed=true;
				}
				bufferedReader.close();
				fileReader.close();
				if (zoomed==true){
					PrintWriter writer = new PrintWriter("tmp.txt", "UTF-8");
					writer.print("0");
					writer.close();
				}
			}catch(IOException ex){
				System.out.println("File not found");
			}
			}
	    }
	}
	//zooming
    private class ZoomingPane extends Pane {
        Node content;
        private DoubleProperty zoomFactor = new SimpleDoubleProperty(1);

        private ZoomingPane(Node content) {
            this.content = content;
            getChildren().add(content);
            Scale scale = new Scale(1, 1);
            content.getTransforms().add(scale);

            zoomFactor.addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    scale.setX(newValue.doubleValue());
					//scale.setX(scale.getX());
                    scale.setY(newValue.doubleValue());
					//scale.setY(scale.getY());
                    requestLayout();
					thumb=newValue.doubleValue();
                }
            });
        }

        protected void layoutChildren() {
            Pos pos = Pos.TOP_LEFT;
            double width = getWidth();
            double height = getHeight();
            double top = getInsets().getTop();
            double right = getInsets().getRight();
            double left = getInsets().getLeft();
            double bottom = getInsets().getBottom();
            double contentWidth = (width - left - right)/zoomFactor.get();
			//double contentWidth = (width-left-right)/1.5;
			//double contentHeight= (height-top-bottom)/1.5;
            double contentHeight = (height - top - bottom)/zoomFactor.get();
            layoutInArea(content, left, top,
                    contentWidth, contentHeight,
                    0, null,
                    pos.getHpos(),
                    pos.getVpos());
        }

        public final Double getZoomFactor() {
            return zoomFactor.get();
		   //return 0.0;
        }
        public final void setZoomFactor(Double zoomFactor) {
            this.zoomFactor.set(zoomFactor);
        }
        public final DoubleProperty zoomFactorProperty() {
            return zoomFactor;
        }
    }
	
	private EventHandler<ActionEvent> test2 = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			th.start();
		}
	};
	private EventHandler<ActionEvent> test = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			handler.cancel();
		}
	};

	private EventHandler<ActionEvent> reload = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			webEngine.reload();
			resetButtons();
		}
	};

	private EventHandler<ActionEvent> go = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			String address = null;
			loadRandomAddress(address);
		}
	};
	
	private void loadRandomAddress(String address) {
		if(address == null)
			address = addressField.getText();
		address = extractAddress(address);
		System.out.println(address);
		webEngine.load("http://" + address);
		addressField.setText(webEngine.getLocation());

		removeObsoleteAddresses();
		addresses.add(address);
		resetButtons();
	}

	private void removeObsoleteAddresses() {
		addressPointer++;
		while (addresses.size() - 1 >= addressPointer) {
			addresses.remove(addresses.size() - 1);
		}
	}
	
	private EventHandler<ActionEvent> back = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {

			addressPointer--;
			if (addressPointer >= 0) {
				loadPointedAddress();
			} else {
				addressPointer = 0;
			}
			resetButtons();
		}
	};

	private EventHandler<ActionEvent> forward = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {

			addressPointer++;
			if (addressPointer <= addresses.size() - 1) {
				loadPointedAddress();
			} else {
				addressPointer = addresses.size() - 1;
			}
			resetButtons();
		}
	};
	private EventHandler<ActionEvent> set = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			thumb+=0.1;
			slider.setValue(thumb);
			resetButtons();
		}
	};
	
	private EventHandler<ActionEvent> reset = new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			thumb-=0.1;
			slider.setValue(thumb);
			resetButtons();
		}
	};

	private void loadPointedAddress() {
		System.out.println(addresses.get(addressPointer));
		webEngine.load("http://" + addresses.get(addressPointer));
		addressField.setText(webEngine.getLocation());
	}

	private void resetButtons() {
		System.out.println(addresses);
		System.out.println(addressPointer);

		if (addressPointer <= 0)
			backButton.setDisable(true);
		else
			backButton.setDisable(false);

		if (addressPointer >= addresses.size() - 1)
			forwardButton.setDisable(true);
		else
			forwardButton.setDisable(false);
	}

	private String extractAddress(String fullAddress) {
		String result = fullAddress;
		if (fullAddress.startsWith("http://") ) {
			result = fullAddress.substring("http://".length());
		}
		else if (fullAddress.startsWith("https://")) {
			result = fullAddress.substring("https://".length());
		} 
		return result;
	}

	public static void main(String[] args) {
		launch(args);
	}
}